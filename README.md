# DOOM Project
We want to build an AI to play Doom, a popular FPS game, that is able to navigate a known map and shoot at in-game enemies, and perhaps able to beat amateur human players. It offers the ability to train AI agents with reinforcement learning, more specifically with Deep Q-Networks (DQN) and Deep Recurrent Q-Networks (DRQN) algorithms.

## Write-Up
The write-up can be found in writeup.pdf.

## Installation

#### Dependencies
- Python 3 with NumPy, scikit-image, tqdm
- PyTorch
- ViZDoom (Follow https://github.com/mwydmuch/ViZDoom or https://github.com/mwydmuch/ViZDoom/blob/master/doc/Quickstart.md for more details)

## Code structure

    .
    ├── agents              
        ├── examples                # Examples of pretrained models
    ├── resources
        ├── basic.cfg               # Basic scenario from ViZDoom
        ├── basic.wad
        ├── monster.cfg             # Customized scenario monster for shooting
        ├── monster.wad
        ├── health_gathering.cfg    # Health_gathering scenario from ViZDoom for navigation
        ├── health_gathering.wad
    ├── src                         # Source code
        └── ...
    ├── main.py                     # Main file
    ├── graph_results.py            # To obtain resulting graph for performance analysis with pretrained models
    └── writeup.pdf                 # Write-up of the project
    └── README.md

## Usage

### Human player mode

The parameters that is used to collect human play data can be passed through command line:

```bash
python main.py

--scenario "monster" 		    # scenario of the game (listed in resource folder)
--human_play "true"		    # use spectator mode of the game to allow human playing the game
```

Here are some sample commands to get human play data of certain scenario:
```bash
python3 main.py --scenario monster --human_play true
```

Example output:
Should open up a game window, playable with keyboard and mouse. The reward is printed through the terminal as follows:

```
Episode #1
Episode finished!
Total reward: -5.0
Total kills: 0.0
Total time survived: 57
************************
...
```

### Training mode

The parameters that is used to tune a training model can be passed through command line:

```bash
python main.py

## Game mode
--scenario "monster" 		        # scenario of the game (listed in resource folder)
--agent_name new_agent		 	# name for the training agent, creating an agent with the same name will overwrite the original data

## General parameters about the game
--gray "true"                   	# use grayscale screen
--use_screen_buffer "true"       	# use the screen buffer (what the player sees)
--use_depth_buffer "false"       	# use the depth buffer

## Learning algorithm
--algorithm "dnn"        	 	# learning algorithm type (dnn/rnn)

## Hyper-parameters
--learning_rate 0.00001			# learning rate
--learning_steps 2000                   # learning steps per epoch  
--discount_rate 0.99			# discount rate
--replay_memory_size 10000     		# maximum number of frames in the replay memory
--batch_size 64                  	# batch size
--trace_size 1                          # trace size
--frame_skip 8                          # frame skip
--epochs 50			        # total number of epochs
--validation_episodes 100		# number of validation episodes per epoch
--visualization_episodes 10             # number of episodes to watch per epoch
```

Here are some examples of the training commands:
```bash
python3 main.py --scenario monster --frame_skip 4 --epochs 25
```

Example output:
The stats of each training epochs should be displayed as it progress. The final results of the agent will be displayed through a game window at the end of the training.

```
Initializing doom...
Doom initialized.
Starting the training with dnn!

Epoch 1
-------
Training...
194 training episodes played.
Scores: mean: -4.8 +/- 2.6, min: -10.0, max: 3.0,

Testing...
Scores: mean: -4.5 +/- 2.5, min: -8.0, max: 1.0                                                     
Time survived: mean: 52.6 +/- 15.6, min: 48.0 max: 136.0
KIll count: mean: 1.1 +/- 0.9, min: 0.0 max: 3.0
Total elapsed time: 0.50 minutes
Saving the results to:...
```

### Visualization mode

Once the agent is trained, you can visualize it by running the same command, and using the following extra arguments:

```bash
--visualize true              	        # visualize the model (render the screen)
--agent PATH                   	        # path where the trained agent was saved
```

Sample commands to visualize pretrained model: (the scenatio must match with the trained agents)
```bash
python3 main.py --scenario monster --visualize true --agent agents/examples/DNN_frameskip_16_monster.pth
python3 main.py --scenario health_gathering --visualize true --agent agents/examples/DNN_health_gathering.pth
```

Example output:
Should open up a game window to watch how the agent perform in a given scenario. The performance measurement is printed through the terminal as follows:

```
Training finished. It's time to watch!
Total score:  0.0
Total score:  -2.0
...
```

## Acknowledgements

Some of the maps and wad files have been borrowed from the [ViZDoom](https://github.com/mwydmuch/ViZDoom). The DNN algorithm implementation is build upon the PyTorch exmaple from the [ViZDoom](https://github.com/mwydmuch/ViZDoom).
