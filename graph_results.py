# usage: python graph_results.py [path to .pth.npz file]
from os import path
import numpy as np
import matplotlib.pyplot as plt
import sys

assert len(sys.argv) == 2, "usage: python graph_results.py [path to .pth.npz file]"

filepath = sys.argv[1]
assert path.exists(filepath), "file " + filepath + " does not exist"

data = np.load(filepath, allow_pickle=True)
arr = data['arr_0']

train_episodes = arr[0]
total_train_episodes = np.cumsum(train_episodes)
epochs = range(1,train_episodes.shape[0] + 1)
train_time = arr[1]
train_scores = arr[2]
test_scores = arr[3]
test_results = arr[4]

fig, axs = plt.subplots(nrows=3)
fig.set_size_inches(12, 20)

x_labels = ['time elapsed (s)', 'episodes elapsed', 'epochs elapsed']
x_vals = np.vstack((train_time, total_train_episodes, epochs))

for i, ax in enumerate(axs):
    ax.set_ylabel('reward')
    ax.set_xlabel(x_labels[i])
    ax.errorbar(x_vals[i], train_scores[0], yerr=train_scores[1], elinewidth=0.2, label='train')
    ax.errorbar(x_vals[i], test_scores[0], yerr=test_scores[1], elinewidth=0.2, label='test')
    ax.legend(loc='upper left')

fig.savefig(filepath.rstrip('pth.npz') + '_rewards')

fig, axs = plt.subplots(nrows=3)
fig.set_size_inches(12, 20)
for i, ax in enumerate(axs):
    ax.set_ylabel('result')
    ax.set_xlabel(x_labels[i])
    ax.errorbar(x_vals[i], test_results[0], yerr=test_results[1], elinewidth=0.2)
fig.savefig(filepath.rstrip('pth.npz') + '_results')