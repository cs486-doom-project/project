import argparse
import os
import random
from src.cmdlineparser import str_to_bool, parse_arguments
import src.spectator
import subprocess
import vizdoom

def create_directory(directory):
    assert len(directory) > 0
    if not os.path.isdir(directory):
        subprocess.Popen("mkdir %s" % directory.replace(" ", "\ "), shell = True).wait()

# Create a directory for the agent
def create_agent_dir(exp_name):
    root_dir = os.path.dirname(os.path.abspath(__file__))
    root_path = os.path.join(root_dir, 'agents')
    assert len(exp_name) > 0
    create_directory(root_path)
    agent_path = os.path.join(root_path, exp_name)
    create_directory(agent_path)
    return agent_path

parser = argparse.ArgumentParser(description='Starting a game with VizDoom')
parser.add_argument("--agent_name", type = str, default = "default", help = "Agent name, will overwrite if share the same name")
parser.add_argument("--scenario", type=str, default="monster", help="Scenario of the DOOM game")
parser.add_argument("--human_play", type=str_to_bool, default=False, help="Allows human to play the game (spectator mode)")
parser.add_argument("--visualize", type=str_to_bool, default=False, help="Visualize trained model")
parser.add_argument("--agent", type=str, default="", help="Path to a trained model")
args, remaining = parser.parse_known_args()

assert os.path.isfile("resources/" + args.scenario + ".cfg")
if (args.human_play):
    src.spectator.start_human_play(args.scenario)
else:
    assert len(args.agent_name.strip()) > 0
    # create the directory and logger for the agent
    agent_path = create_agent_dir(args.agent_name)
    if (args.visualize):
        assert not args.visualize or os.path.isfile(args.agent)
        src.cmdlineparser.parse_arguments(remaining, args.agent, args.scenario, args.visualize)
    else:
        src.cmdlineparser.parse_arguments(remaining, agent_path, args.scenario, args.visualize)