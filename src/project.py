from vizdoom import *
import itertools as it
from time import time, sleep
import numpy as np
import skimage.color, skimage.transform
import torch
import os
from tqdm import trange
import vizdoom as vzd

from src.model import DNN, RNN
from src.replaymemory import ReplayMemory
from src.trainer import Trainer

# Load from GPU if possible
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

# Scenario / config path
scenario = "resources/basic.cfg"
# One of 'gray', 'rgb', 'depth'
screen_format = 'gray'

# Q-learning settings
learning_rate = 0.00025
discount_factor = 0.99
epochs = 50
learning_steps_per_epoch = 2000
replay_memory_size = 10000

# NN learning settings
batch_size = 64
trace_size = 1

# Training regime
test_episodes_per_epoch = 100

# Other parameters
frame_repeat = 8
resolution = (30, 45)
episodes_to_watch = 10

model_savefile = "./model-doom.pth"
skip_learning = False
learning_model = "dnn"

# Converts and down-samples the input image
def get_state(game, n_channel, use_depth=False):
    img = game.get_state().depth_buffer if use_depth else game.get_state().screen_buffer
    if n_channel != 1:
        # skimage.transform.resize requires the channel axis to be last:
        # shape: (3, 120, 160) => (120, 160, 3)
        img = np.transpose(img, axes=[1, 2, 0])
    img = skimage.transform.resize(img, resolution)
    img = img.astype(np.float32)
    if n_channel != 1:
        # shape: (30, 45, 3) => (3, 30, 45)
        img = np.transpose(img, axes=[2, 0, 1])
    return img

def initialize_parameters(parameters, agent_path, model_scenario, watch):
    global scenario, model_savefile, skip_learning, screen_format
    if (watch):
        model_savefile = agent_path
    else:
        model_savefile = os.path.join(agent_path, model_scenario + ".pth")
    scenario = "resources/" + model_scenario + ".cfg"
    skip_learning = watch

    if (parameters.use_screen_buffer and parameters.gray):
        screen_format = "gray"
    elif (parameters.use_screen_buffer):
        screen_format = "rgb"
    else:
        screen_format = "depth"

    global learning_rate, discount_factor, epochs, learning_steps_per_epoch, replay_memory_size
    learning_rate = parameters.learning_rate
    discount_factor = parameters.discount_rate
    epochs = parameters.epochs
    learning_steps_per_epoch = parameters.learning_steps
    replay_memory_size = parameters.replay_memory_size

    global batch_size, trace_size, test_episodes_per_epoch, episodes_to_watch, frame_repeat, learning_model
    batch_size = parameters.batch_size
    trace_size = parameters.trace_size
    test_episodes_per_epoch = parameters.validation_episodes
    frame_repeat = parameters.frame_skip
    learning_model = parameters.algorithm.lower() 

    episodes_to_watch = parameters.visualization_episodes

# Creates and initializes ViZDoom environment.
def initialize_vizdoom(scenario, screen_format):
    print("Initializing doom...")
    game = DoomGame()
    game.load_config(scenario)
    game.set_window_visible(False)
    game.set_mode(Mode.PLAYER)

    # Set screen format.
    if screen_format == 'gray':
        game.set_screen_format(ScreenFormat.GRAY8)
    elif screen_format == 'rbg':
        game.set_screen_format(ScreenFormat.CRCGCB)
    elif screen_format == 'depth':
        game.set_screen_format(ScreenFormat.GRAY8)
        game.set_depth_buffer_enabled(True)

    # Lowest possible render size!
    game.set_screen_resolution(ScreenResolution.RES_160X120)

    # IF WE RAN OUT OF TRAINING TIME, we can reduce the cluttering on the screen:
    # game.set_render_hud(False)
    # game.set_render_minimal_hud(False)
    # game.set_render_crosshair(False)
    # game.set_render_weapon(False)
    # game.set_render_decals(False)
    # game.set_render_particles(False)
    # game.set_render_effects_sprites(False)
    # game.set_render_messages(False)
    # game.set_render_corpses(False)
    # game.set_render_screen_flashes(False)

    game.init()
    print("Doom initialized.")
    return game

def perform_learning_step(n_channel, use_depth):
    global game, actions, memory, trainer, model
    # Preprocessing the current frame into a state
    if not isinstance(model, DNN):
        h = trainer.hidden.detach()
    s1 = get_state(game, n_channel, use_depth)
    s1 = np.reshape(s1, (1, n_channel, resolution[0], resolution[1]))

    # Evaluate and execute the action from Trainer
    a = trainer.get_action(s1, len(actions))
    reward = game.make_action(actions[a], frame_repeat)
    not_terminal = not game.is_episode_finished()
    s2 = get_state(game, n_channel, use_depth) if not_terminal else None

    # Save the transition in ReplayMemory
    memory.add_transition(s1, a, s2, not_terminal, reward, None if isinstance(model, DNN) else h)
    trainer.learn(batch_size)

def save_results_per_epoch(a, l, epoch):
    l = np.array(l)
    # 0: mean; 1: std; 2: max; 3: min
    a[0][epoch] = l.mean()
    a[1][epoch] = l.std()
    a[2][epoch] = l.min()
    a[3][epoch] = l.max()

def load_the_game(params, agent_path, model_scenario, watch = False):
    global game, actions, memory, trainer, model
    # Initialize hyperparameters
    initialize_parameters(params, agent_path, model_scenario, watch)
    # Create Doom instance
    game = initialize_vizdoom(scenario, screen_format)
    use_depth = screen_format == 'depth'
    n_channel = 3 if screen_format == 'rgb' else 1

    # Action = which buttons are pressed
    n = game.get_available_buttons_size()
    actions = [list(a) for a in it.product([0, 1], repeat=n)]
    
    # Set up the dnn model, replay memory and trainer
    if (skip_learning):
        model = torch.load(model_savefile, map_location=device)
    elif (learning_model == "dnn"):
        model = DNN(n_channel, len(actions))
    else:
        model = RNN(n_channel, len(actions))
    memory = ReplayMemory(replay_memory_size, n_channel, resolution[0], resolution[1], not isinstance(model, DNN))
    trainer = Trainer(memory, model, learning_rate, discount_factor)

    # Save results
    np_savefile = model_savefile + '.npz'
    train_episodes = np.zeros(epochs)
    train_time = np.zeros(epochs)
    # 0: mean; 1: std; 2: max; 3: min
    train_score_per_epoch = np.zeros((4, epochs))
    test_score_per_epoch = np.zeros((4, epochs))
    test_time_survived_per_epoch = np.zeros((4, epochs))
    test_kill_count_per_epoch = np.zeros((4, epochs))

    print("Starting the training with " + learning_model + "!")
    time_start = time()
    max_test_score_mean = float('-inf')
    if not skip_learning:
        for epoch in range(epochs):
            print("\nEpoch %d\n-------" % (epoch + 1))
            train_episodes[epoch] = 0
            train_scores = []

            print("Training...")
            game.new_episode()
            trainer.new_episode()
            for learning_step in trange(learning_steps_per_epoch, leave=False):
                perform_learning_step(n_channel, use_depth)
                if game.is_episode_finished():
                    score = game.get_total_reward()
                    train_scores.append(score)
                    game.new_episode()
                    trainer.new_episode()
                    train_episodes[epoch] += 1

            print("%d training episodes played." % train_episodes[epoch])

            train_scores = np.array(train_scores)
            save_results_per_epoch(train_score_per_epoch, train_scores, epoch)
            print("Scores: mean: %.1f +/- %.1f," % (train_score_per_epoch[0][epoch], train_score_per_epoch[1][epoch]), \
                  "min: %.1f," % train_score_per_epoch[2][epoch], "max: %.1f," % train_score_per_epoch[3][epoch])

            print("\nTesting...")
            test_scores = []
            test_time_survived = []
            test_kill_count = []
            for test_episode in trange(test_episodes_per_epoch, leave=False):
                game.new_episode()
                trainer.new_episode()
                time_survived = 0
                while not game.is_episode_finished():
                    state = get_state(game, n_channel, use_depth)
                    state = state.reshape([1, n_channel, resolution[0], resolution[1]])
                    best_action_index = trainer.get_action(state, len(actions), epsilon_greedy=False)
                    game.make_action(actions[best_action_index], frame_repeat)
                    time_survived += frame_repeat
                test_scores.append(game.get_total_reward())
                # Custom result that is seperate from reward.
                # Defined in each scenario as vzd.GameVariable.USER1
                # (# of kills for "monster"; # of tics survived for "health_gathering")
                test_time_survived.append(time_survived)
                test_kill_count.append(game.get_game_variable(GameVariable.KILLCOUNT))

            test_scores = np.array(test_scores)
            save_results_per_epoch(test_score_per_epoch, test_scores, epoch)
            print("Scores: mean: %.1f +/- %.1f," % (test_score_per_epoch[0][epoch], test_score_per_epoch[1][epoch]), \
                "min: %.1f" % test_score_per_epoch[2][epoch], "max: %.1f" % test_score_per_epoch[3][epoch])
            save_results_per_epoch(test_time_survived_per_epoch, test_time_survived, epoch)
            print("Time survived: mean: %.1f +/- %.1f," % (test_time_survived_per_epoch[0][epoch], test_time_survived_per_epoch[1][epoch]), \
                "min: %.1f" % test_time_survived_per_epoch[2][epoch], "max: %.1f" % test_time_survived_per_epoch[3][epoch])
            save_results_per_epoch(test_kill_count_per_epoch, test_kill_count, epoch)
            print("KIll count: mean: %.1f +/- %.1f," % (test_kill_count_per_epoch[0][epoch], test_kill_count_per_epoch[1][epoch]), \
                "min: %.1f" % test_kill_count_per_epoch[2][epoch], "max: %.1f" % test_kill_count_per_epoch[3][epoch])

            train_time[epoch] = time() - time_start
            print("Total elapsed time: %.2f minutes" % (train_time[epoch] / 60.0))
            
            print("Saving the results to:", np_savefile)
            np.savez(np_savefile, (train_episodes, train_time, train_score_per_epoch, test_score_per_epoch, test_time_survived_per_epoch, test_kill_count_per_epoch))
            
            print("Saving lastest to:", model_savefile + ".latest")
            torch.save(model, model_savefile + ".latest")

            if (max_test_score_mean < test_score_per_epoch[0][epoch]):
                print("Best score! Saving the network weigths to:", model_savefile)
                torch.save(model, model_savefile)
                max_test_score_mean = test_score_per_epoch[0][epoch]
            trainer.epoch += 1

    game.close()
    print("======================================")
    print("Training finished. It's time to watch!")

    # Reinitialize the game with window visible
    game.set_window_visible(True)
    # game.set_mode(Mode.ASYNC_PLAYER)
    game.init()

    for _ in range(episodes_to_watch):
        game.new_episode()
        trainer.new_episode()
        while not game.is_episode_finished():
            state = get_state(game, n_channel, use_depth)
            state = state.reshape([1, n_channel, resolution[0], resolution[1]])
            best_action_index = trainer.get_action(state, len(actions), epsilon_greedy=False)

            # Instead of make_action(a, frame_repeat) in order to make the animation smooth
            game.set_action(actions[best_action_index])
            for _ in range(frame_repeat):
                sleep(0.0286)
                game.advance_action()

        # Sleep between episodes
        sleep(1.0)
        score = game.get_total_reward()
        print("Total score: ", score)