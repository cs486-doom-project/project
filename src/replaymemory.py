import numpy as np
import torch
from random import sample

# Save to GPU if possible
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

# Replay memory implementation
class ReplayMemory:
    # capacity: maximum number of transitions stored
    # n_channel: number of channels of each frame
    # height: height of each frame
    # width: width of each frame
    def __init__(self, capacity, n_channel, height, width, save_hidden_state=False):
        self.states_shape = (capacity, n_channel, height, width)
        self.s1 = torch.zeros(self.states_shape, dtype=torch.float32, device=device)
        self.s2 = torch.zeros(self.states_shape, dtype=torch.float32, device=device)
        self.a = torch.zeros(capacity, dtype=torch.int64, device=device)
        self.r = torch.zeros(capacity, dtype=torch.float32, device=device)
        self.not_terminal = torch.zeros(capacity, dtype=torch.bool, device=device)

        self.save_hidden_state = save_hidden_state
        if self.save_hidden_state:
            self.hidden_states_shape = (capacity, 128)
            self.h = torch.zeros(self.hidden_states_shape, dtype=torch.float32, device=device)

        self.capacity = capacity
        self.size = 0
        self.pos = 0

    # Add a transition to the memory.
    # s1: previous state
    # action: action taken at previous state
    # s2: next state
    # not_terminal: whether the action terminates the episode
    # reward: reward from the action
    def add_transition(self, s1, action, s2, not_terminal, reward, h=None):
        s1 = torch.from_numpy(s1).to(device)
        self.s1[self.pos, :, :, :] = s1
        self.a[self.pos] = action
        if not_terminal:
            s2 = torch.from_numpy(s2).to(device)
            self.s2[self.pos, :, :, :] = s2
        self.not_terminal[self.pos] = not_terminal
        self.r[self.pos] = reward
        if self.save_hidden_state:
            self.h[self.pos] = h

        self.pos = (self.pos + 1) % self.capacity
        self.size = min(self.size + 1, self.capacity)

    # Retrieve transitions from the memory.
    # For DNN, batch_size # of transitions are retrieved.
    def get_sample(self, batch_size):
        i = sample(range(0, self.size), batch_size)
        if self.save_hidden_state:
            return self.s1[i], self.a[i], self.s2[i], self.not_terminal[i], self.r[i], self.h[i]
        else:
            return self.s1[i], self.a[i], self.s2[i], self.not_terminal[i], self.r[i]