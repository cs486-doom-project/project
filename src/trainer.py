import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from random import randint, random
from torch.autograd import Variable

from src.replaymemory import ReplayMemory
from src.model import DNN

# Set to GPU if possible
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

class Trainer:
    # memory: ReplayMemory
    # model: neural network model
    # learning_rate: learning rate for gradient descent
    # discount_factor: discount factor of Q-value
    # starting_epoch: starting epoch (for calculating epsilon)
    def __init__(self, memory, model, learning_rate, discount_factor, starting_epoch=0):
        self.memory = memory
        self.model = model.to(device)
        self.discount_factor = discount_factor
        self.epoch = starting_epoch
        self.criterion = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), learning_rate)
        self.is_feed_forward = isinstance(self.model, DNN)

    def new_episode(self):
        if not self.is_feed_forward:
            self.hidden = self.model.init_hidden(1)

    ### TRAINING ###
    # Learn for one learning step (batch_size #s of transitions).
    # batch_size: amounts of transitions to train
    def learn(self, batch_size):
        if self.memory.size < batch_size:
            # Memory does not have enough transitions for training yet!
            return

        if self.is_feed_forward:
            s1, a, s2, not_terminal, r = self.memory.get_sample(batch_size)
            s1_output = self.model(s1)
            s2_output = self.model(s2)
        else:
            s1, a, s2, not_terminal, r, h = self.memory.get_sample(batch_size)
            s1_output, h1 = self.model(s1, h)
            s2_output, _ = self.model(s2, h1.detach())
            # Flatten the first dimension which was created by the RNN layer:
            # shape: (1, 16, 8) -> (16, 8)
            if len(s1_output.shape) == 3:
                s1_output = torch.flatten(s1_output, start_dim=0, end_dim=1)
                s2_output = torch.flatten(s2_output, start_dim=0, end_dim=1)

        # Calculate the target Q-value
        max_q, _ = torch.max(s2_output.detach(), 1)
        target_q = torch.clone(s1_output).detach()
        target_q[range(target_q.shape[0]), a] = r + self.discount_factor * not_terminal * max_q
        target_q = Variable(target_q).detach()

        # Gradient descent
        loss = self.criterion(s1_output, target_q)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss

    ### EVALUATING ###
    # Get action for a state based on epsilon-greedy.
    def get_action(self, state, action_count, epsilon_greedy=True):
        # Return the current epsilon value for this epoch.
        # Currently set as decaying from 1.0 to 0.1 like this: ‾\_
        def epsilon():
            start_eps = 1.0
            end_eps = 0.1
            const_eps_epochs = 10
            eps_decay_epochs = 40

            if self.epoch < const_eps_epochs:
                return start_eps
            elif self.epoch < eps_decay_epochs:
                # Linear decay
                return start_eps - (self.epoch - const_eps_epochs) / \
                                (eps_decay_epochs - const_eps_epochs) * (start_eps - end_eps)
            else:
                return end_eps

        if epsilon_greedy and random() <= epsilon():
            if not self.is_feed_forward:
                state = torch.from_numpy(state).to(device)
                _, self.hidden = self.model(state, self.hidden)
            # Return a random action.
            return randint(0, action_count - 1)
        else:
            # Return the best action evaluated by the model.
            state = torch.from_numpy(state).to(device)
            if self.is_feed_forward:
                q = self.model(state)
            else:
                q, self.hidden = self.model(state, self.hidden)
                # Flatten the first dimension which was created by the RNN layer:
                # shape: (1, 1, 8) -> (1, 8)
                if len(q.shape) == 3:
                    q = torch.flatten(q, start_dim=0, end_dim=1)
            _, index = torch.max(q, 1)
            action = index.cpu().data.numpy()[0].item()
            return action
