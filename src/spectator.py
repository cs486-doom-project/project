#!/usr/bin/env python3
# This file is from VizDoom examples: https://github.com/mwydmuch/ViZDoom/blob/master/examples/python/spectator.py
#####################################################################
# This script presents SPECTATOR mode. In SPECTATOR mode you play and
# your agent can learn from it.
# Configuration is loaded from "resources/<SCENARIO_NAME>.cfg" file.
# 
# To see the scenario description go to "README.md"
#####################################################################

from __future__ import print_function

from vizdoom import *
from time import sleep
import vizdoom as vzd
from argparse import ArgumentParser

def start_human_play(scenario):
    scenario_config = "resources/" + scenario + ".cfg"
    game = vzd.DoomGame()

    # Choose scenario config file you wish to watch.
    # Don't load two configs cause the second will overrite the first one.
    # Multiple config files are ok but combining these ones doesn't make much sense.

    game.load_config(scenario_config)

    # Enables freelook in engine
    game.add_game_args("+freelook 1")

    game.set_screen_resolution(vzd.ScreenResolution.RES_640X480)

    # Enables spectator mode, so you can play. Sounds strange but it is the agent who is supposed to watch not you.
    game.set_window_visible(True)
    game.set_mode(vzd.Mode.SPECTATOR)

    game.init()

    episodes = 10

    for i in range(episodes):
        print("Episode #" + str(i + 1))

        game.new_episode()
        while not game.is_episode_finished():
            state = game.get_state()

            game.advance_action()
            last_action = game.get_last_action()
            reward = game.get_last_reward()

            # print("State #" + str(state.number))
            # print("Game variables: ", state.game_variables)
            # print("Action:", last_action)
            # print("Reward:", reward)
            # print("=====================")

        print("Episode finished!")
        print("Total reward:", game.get_total_reward())
        print("Total kills:", game.get_game_variable(GameVariable.KILLCOUNT))
        print("Total time survived:", game.get_episode_time())
        print("************************")
        sleep(2.0)

    game.close()
