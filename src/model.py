import torch
import torch.nn as nn
import torch.nn.functional as F

# Save to GPU if possible
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

# DNN implementation
class DNN(nn.Module):
    # input_channel: # of input channels
    # output_count: # of output nodes of the network (action count)
    def __init__(self, input_channel, output_count):
        super(DNN, self).__init__()
        self.conv1 = nn.Conv2d(input_channel, 8, kernel_size=6, stride=3)
        self.conv2 = nn.Conv2d(8, 8, kernel_size=3, stride=2)
        self.fc1 = nn.Linear(192, 128)
        self.fc2 = nn.Linear(128, output_count)

    # x: input
    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 192)
        x = F.relu(self.fc1(x))
        return self.fc2(x)

# RNN implementation; should be the same as DNN except for GRU
# If you change anything in DNN, you should change it here too!
class RNN(nn.Module):
    # input_channel: # of input channels
    # output_count: # of output nodes of the network (action count)
    def __init__(self, input_channel, output_count):
        super(RNN, self).__init__()
        self.conv1 = nn.Conv2d(input_channel, 8, kernel_size=6, stride=3)
        self.conv2 = nn.Conv2d(8, 8, kernel_size=3, stride=2)
        self.gru = nn.GRUCell(192, 128)
        self.fc = nn.Linear(128, output_count)

    # x: input
    # h: hidden output
    def forward(self, x, h):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 192)
        h = self.gru(x, h)
        x = F.relu(h)
        return self.fc(x), h

    def init_hidden(self, batch_size):
        return torch.zeros(batch_size, 128, device=device)