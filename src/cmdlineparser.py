import argparse
import os
import src.project

def str_to_bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1', 'on'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0', 'off'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def validate_parameters(params):
    assert params.algorithm.lower() in ["dnn", "rnn"]
    assert params.use_screen_buffer ^ params.use_depth_buffer
    assert params.learning_rate > 0
    assert params.discount_rate > 0
    assert params.learning_steps > 0
    assert params.batch_size > 0
    assert params.trace_size > 0
    assert params.frame_skip > 0
    assert params.epochs > 0
    assert params.validation_episodes > 0
    assert params.visualization_episodes > 0

# Parse the command line arguments and load the game
def parse_arguments(args, agent_path, scenario, visualize = False):
    parser = argparse.ArgumentParser()

    # General parameters about the game rendering
    parser.add_argument("--gray", type=str_to_bool, default=True, help="Use grayscale screen")
    parser.add_argument("--use_screen_buffer", type=str_to_bool, default=True, help="Use the screen buffer")
    parser.add_argument("--use_depth_buffer", type=str_to_bool, default=False, help="Use the depth buffer")

    # Hyper-parameters for learning
    parser.add_argument("--learning_rate", type=float, default=0.00025, help="Learning rate")
    parser.add_argument("--learning_steps", type=int, default=2000, help="Learning steps per epoch")
    parser.add_argument("--discount_rate", type=float, default=0.99, help="Discount factor")
    parser.add_argument("--replay_memory_size", type=int, default=10000, help="Maximum number of frames in the replay memory")
    parser.add_argument("--batch_size", type=int, default=64, help="Batch size")
    parser.add_argument("--trace_size", type=int, default=1, help="Trace size")
    parser.add_argument("--frame_skip", type=int, default=8, help="Frame skip")
    parser.add_argument("--epochs", type=int, default=50, help="Number of epochs")
    parser.add_argument("--validation_episodes", type=int, default=100, help="Validation episodes per epoch")
    parser.add_argument("--visualization_episodes", type=int, default=10, help="Number of episodes to watch per epoch")

    # Learning algorithm
    parser.add_argument("--algorithm", type=str, default="dnn", help="Learning algorithm used for the agent")

    # validate parameters
    parameters, _ = parser.parse_known_args(args)
    validate_parameters(parameters)

    # load the game
    src.project.load_the_game(parameters, agent_path, scenario, visualize)